'''SETTING UP PARAMETERS'''

inhabitants = 5
energy_supplier = 'GSW'             # 'PRW','GSW'
tariff = 'TOU'                     # 'TOU','Flat'
forecast_type = 'SES'           # 'Perfect','Naive','SES'
energieeffizienzklasse = 'C'       # '>=A+','A','B','C','D','E','<=F'
control_type = 'Real'               # 'Real','Conventional','Optimal'
plots = 'OFF'
legionella_safety = 'ON'
analysed_weeks = 52
T_pref = 40.5
T_max = 85
T_in = 10  # inlet temperature
T_setpoint = 57  # for conventional control, thresholds @ +/- 3
T_ambient = 20

Max_Solving_Time = None
Max_Gap = None


import time,matplotlib.pyplot as plt,xlsxwriter,csv
from src import *

'''Tracking computational time'''
start = time.time()

'Training weeks, necessary for exponential smoothing'
training_weeks = 5


T_min =T_pref #default minimum temperature bound is the preferred water temp.; increase when imperfect forecast
weeks = analysed_weeks + training_weeks
counter = 0  # variable counting scenarios to run, used to create a new line in the result excel for every simu
horizon = 0  # no function currently
export_path = (os.getcwd() + '/output')
script_dir = (os.getcwd() + '/input')

# Create an new Excel file and add a worksheet.
workbook = xlsxwriter.Workbook(export_path + '/Excel/' + str(time.time()) + '.xlsx')
sheet1 = workbook.add_worksheet()

days = weeks * 7
time_steps = days * 24 * 60

'''electricity tariff data import'''
importcost = []
name = '/%s_' % tariff
finalname = name + '%s.csv' % energy_supplier
test_file_price = csv.reader(
    open(script_dir + finalname), delimiter=",")
for price_entry in test_file_price:
    importcost.append(float(price_entry[0]))  # ct /kWh

'''dhw consumption data import'''
V_DHW = []
Vol = inhabitants * 100
test_file_dhw = csv.reader(open(script_dir + '/DHW%s.csv' % str(Vol / 2)), delimiter=",")
for dhw_entry in test_file_dhw:
    V_DHW.append(float(dhw_entry[0]))  # kW

'''electric heater power definition according to market analysis'''
if Vol < 100:
    P_max = 2
elif 250 > Vol >= 100:
    P_max = 3
elif 500 > Vol >= 250:
    P_max = 6
elif 600 >= Vol >= 500:
    P_max = 7.5
else:
    print 'ERROR: Simulation customised for residential water tanks below 600 liters. Reduce Volume.'


'''Creation of water tank object'''
WT1 = WaterTank(aid='1',
                volume=Vol * 10 ** -3,
                energieeffizienzklasse=energieeffizienzklasse,
                T_max=T_max,
                T_min=T_min,
                T_in=T_in,
                T_ambient=T_ambient,
                V_DHW=V_DHW, P_max=P_max,
                importcost=importcost,
                tariff=tariff,
                control=control_type,
                number_days=days, resolution=60, T_setpoint_cc=T_setpoint,
                T_pref=T_pref)

'''Optimal control'''
if control_type == 'Optimal':

    s_T_WT1 = [WT1._T_min]
    s_P_EH1 = [0]
    number_of_reschedulings = (analysed_weeks * 10080) / WT1._t_rescheduling_step

    for i in range(number_of_reschedulings):

        t_start = i * WT1._t_rescheduling_step
        t_end = i * WT1._t_rescheduling_step + WT1._schedule_horizon_step

        '''Transforming optimization input for the considered rescheduling step'''

        forecasted_V_DHW = V_DHW[
                           training_weeks * 10080 + t_start:training_weeks * 10080 + t_start + 10080 * 2]  # kW
        importcost_of_step = importcost[
                             training_weeks * 10080 + t_start:training_weeks * 10080 + t_start + 10080 * 2 + 1]

        try:
            HEMS_MILP = pulp.LpProblem('HEMS_MILP',
                                       pulp.LpMinimize)  # initializing milp

            T_WT1, u_EH1 = WT1.optimal_control(T_initial=s_T_WT1[-1],
                                               V_DHW=forecasted_V_DHW,
                                               LP_model=HEMS_MILP,
                                               rescheduling_step=i,
                                               T_history=s_T_WT1[-24 * 7 * 60:],
                                               legionella_safety=legionella_safety,
                                               importcost=importcost_of_step)

            # initializing objective function for every step in the scheduling horizon
            HEMS_MILP += sum(
                [u_EH1[t] * importcost_of_step[t] for t in range(WT1._schedule_horizon_step)])

            status = HEMS_MILP.solve(
                pulp.GUROBI(mip=True, msg=True, timeLimit=Max_Solving_Time,
                            epgap=Max_Gap))

            print 'MILP status: ' + pulp.LpStatus[status] + ''
            '''status: 1=optimal, 0=Not Solved, -1=Infeasible, -2=Unbounded, -3=Undefined'''
            # Not solved: Is the default setting before a problem has been solved
            # Undefined: Feasible solution hasn't been found (but may exist).

            for t in range(WT1._t_rescheduling_step):
                s_P_EH1.append(P_max * u_EH1[t].value())
                s_T_WT1.append(T_WT1[t].value())

        except pulp.PulpSolverError:
            print 'The simulation crashed at rescheduling step ' + str(i)
            print '\a'

'''Conventional control'''
if control_type == 'Conventional':
    s_T_WT1, s_P_EH1 = WT1.conventional_control(number_steps=time_steps)
    s_T_WT1 = s_T_WT1[training_weeks * 10080:]
    s_P_EH1 = s_P_EH1[training_weeks * 10080:]

'''Real control'''
if control_type == 'Real':
    s_T_WT1, s_P_EH1 = WT1.real_control(number_steps=time_steps,legionella_safety=legionella_safety,forecast_type=forecast_type)
    s_T_WT1 = s_T_WT1[training_weeks * 10080:]
    s_P_EH1 = s_P_EH1[training_weeks * 10080:]


'''Preperation of data for export'''


V_DHW = V_DHW[training_weeks * 10080:]
violations = 0
s_E_c = []
htnt_list = []
T_plot = []
cpp = max(importcost)
nt = min(importcost)

for i in range(2 * 1440):
    if nt < importcost[i] < cpp:
        ht = importcost[i]
        break

for i in range(len(s_P_EH1)):
    impc = importcost[training_weeks * 10080 + i]
    if s_T_WT1[i] < T_pref and V_DHW[i] > 0:
        violations += T_pref - float(s_T_WT1[i])
    s_E_c.append(float(s_P_EH1[i]) / 60 * impc)

''' SAVING DATA '''
T_average = sum(s_T_WT1) / len(s_T_WT1)
E_total = float(sum(s_P_EH1)) / 60
L_total = (T_average - T_ambient) * len(
    s_T_WT1) * WT1._q_loss_per_K / 60
C_total = sum(s_E_c) / 100
T_min_reached = min(s_T_WT1)
Length = len(s_T_WT1)
Calc_time = time.time() - start

'''Plotting'''
if plots == 'ON':
    htnt_list = []
    T_plot = []
    V_plot = []
    cpp = max(importcost)
    nt = min(importcost)
    for i in range(2 * 1440):
        if nt < importcost[i] < cpp:
            ht = importcost[i]
            break

    for n in range(len(s_P_EH1)):
        T_plot.append(s_T_WT1[n] / 10)
        V_plot.append(V_DHW[n] / 100)

        if tariff != 'Flat':

            if importcost[n] == nt:
                htnt_list.append(2)
            elif importcost[n] == cpp:
                htnt_list.append(4)
            else:
                htnt_list.append(3)

    plt.figure()
    plt.plot(V_plot[0:Length], label='V DHW x10^-1 [l/h]')
    plt.plot(s_P_EH1[0:Length], label='P EH  [kW]')
    plt.plot(T_plot[0:Length], label='T WT x10^-1 [C]')
    plt.plot(htnt_list[0:Length], label='HT/NT')
    plt.title('Tariff=' + tariff + ', Volume=' + str(
        Vol) + ',  Control=' + str(
        control_type) + ',  EEC=' + energieeffizienzklasse + ', Forecast=' + str(
        forecast_type))
    plt.legend()


'''XLS FILE'''
sheet1.write(0, 0, 'Residents')
sheet1.write(0, 1, 'control')
sheet1.write(0, 2, 'forecast')
sheet1.write(0, 3, 'tariff')
sheet1.write(0, 4, 'energieeffizienzklasse')
sheet1.write(0, 5, 'T_min')
sheet1.write(0, 6, 'C_total')
sheet1.write(0, 7, 'Calc_time')
sheet1.write(0, 8, 'Violations')
sheet1.write(0, 9, 'T_pref')
sheet1.write(1, 0, int(Vol / 100))
sheet1.write(1, 1, control_type)
sheet1.write(1, 2, forecast_type)
sheet1.write(1, 3, str(tariff) + str(energy_supplier))
sheet1.write(1, 4, energieeffizienzklasse)
sheet1.write(1, 5, T_min)
sheet1.write(1, 6, C_total)
sheet1.write(1, 7, Calc_time)
sheet1.write(1, 8, violations)
sheet1.write(1, 9, T_pref)

workbook.close()

if plots == 'ON':
    plt.show()