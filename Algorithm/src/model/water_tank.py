# -*- coding: utf-8 -*-

import logging
import pulp
import math
import csv
import time
import os
import numpy as np
import pickle
from bisect import bisect_left
import random


log = logging.getLogger(__name__)

class WaterTank(object):

    def T_out(self,T):
        if T >= self._T_pref:
            T_out = self._T_pref
        else:
            T_out = T
        return T_out

    '''parameterization'''

    def __init__(self, aid, volume, energieeffizienzklasse, T_max, T_min, T_in, T_ambient, V_DHW, P_max,
                 tariff, importcost, control, number_days, resolution, T_setpoint_cc, T_pref):
        self._T_pref = T_pref
        self._T_setpoint_cc = T_setpoint_cc
        self._T_upr_threshold = T_setpoint_cc + 3
        self._T_low_threshold = T_setpoint_cc - 3
        self._aid = aid
        self._schedule_horizon_step = 2 * 10080  # number of time step in the considered horizon   2R
        self._time_steps = number_days * 1440
        self._resolution = resolution
        self._t_rescheduling_step = 10080
        self._volume = volume  # m³
        self._tariff = tariff
        self._energieeffizienzklasse = energieeffizienzklasse
        self._V_DHW = list(V_DHW)
        self._P_max = P_max
        self._importcost = list(importcost)
        self._mcp = self._volume * 1000 * 0.9832 * 4.184
        self._rhocp = 0.9832 * 4.184
        self._control = control

        '''Calculation of losses'''
        # in [kW/ΔK] (ΔK = difference between ambient-watertank temperatures)

        if energieeffizienzklasse == '>=A+':
            k = 5.5
            l = 3.16
        elif energieeffizienzklasse == 'A':
            k = 7
            l = 3.705
        elif energieeffizienzklasse == 'B':
            k = 10.25
            l = 5.09
        elif energieeffizienzklasse == 'C':
            k = 14.33
            l = 7.13
        elif energieeffizienzklasse == 'D':
            k = 18.83
            l = 9.333
        elif energieeffizienzklasse == 'E':
            k = 23.5
            l = 11.995
        elif energieeffizienzklasse == '<=F':
            k = 28.5
            l = 15.16

        else:
            print "Energieeffizienzklasse %s doesn't exist" % energieeffizienzklasse
            print '\a'  # beep
            exit()

        self._k = k
        self._l = l
        self._q_loss_per_K = (k + l * (volume * 1000) ** 0.4) / ((65 - 20) * 1000)  # kW/K

        # buffers compensating for the 60s inaccuacies
        self._T_upper_buffer = self._P_max * resolution / self._mcp
        self._T_lower_buffer = (T_min - T_ambient) * self._q_loss_per_K * resolution / self._mcp

        if self._control == 'Real':
            self._T_max = T_max - self._T_upper_buffer  # Kelvin
            self._T_min = T_min + self._T_lower_buffer
        else:
            self._T_max = T_max  # Kelvin
            self._T_min = T_min
        self._T_in = T_in

        self._T_ambient = T_ambient


    def optimal_control(self, T_initial, V_DHW, LP_model, rescheduling_step, T_history,
                                 legionella_safety, importcost):

        self._nr_resched = rescheduling_step
        self._LP_model = LP_model

        u_EH = {}
        T_WT = {}
        last_eradication = 0

        if legionella_safety=='ON':
            x = pulp.LpVariable.dicts("x", range(self._schedule_horizon_step), cat=pulp.LpBinary, upBound=1, lowBound=0)


        for t in range(self._schedule_horizon_step):

            if self._tariff =='Flat' or importcost[t] + importcost[t + 1] < 1000:
                u_EH[t] = pulp.LpVariable('u_EH' + self._aid + '_' + str(t) + '', lowBound=0, upBound=1,cat='Binary')
            else:
                u_EH[t] = pulp.LpVariable('u_EH' + self._aid + '_' + str(t) + '', lowBound=0, upBound=0,cat='Binary')
            T_WT[t] = pulp.LpVariable('T_WT' + self._aid + '_' + str(t) + '', lowBound=self._T_min, upBound=self._T_max,cat='Continuous')


            if legionella_safety == 'ON':
                self._LP_model += T_WT[t]>=x[t]*60

            if t==0:
                self._LP_model += T_WT[t] ==  T_initial + (u_EH[t]*self._P_max - self._rhocp*(40.5-self._T_in)*V_DHW[t]/3600 - self._q_loss_per_K*(T_initial - self._T_ambient))*self._resolution / self._mcp

            else:
                self._LP_model += T_WT[t] == T_WT[t - 1] + (u_EH[t]*self._P_max  - self._rhocp*(40.5-self._T_in)*V_DHW[t]/3600 - self._q_loss_per_K*(T_WT[t - 1] - self._T_ambient))*self._resolution / self._mcp


        if legionella_safety == 'ON':
            for i in range(len(T_history)):
                if T_history[-i] >= 60:
                    j=1
                    while j<=25 and T_history[-j-i]>=60:
                            j+=1
                    if j>=25:
                        last_eradication = i
                    break

        if legionella_safety == 'ON':
            self._LP_model += pulp.lpSum(x[t] for t in range(7*24*60 - last_eradication)) >= 25

        return T_WT,u_EH

    def conventional_control(self,  number_steps):

        T_max_th = self._T_upr_threshold
        T_min_th = self._T_low_threshold

        s_T_WT1_cc = [0.5*(T_max_th + T_min_th)]
        s_P_EH1_cc = [0]

        for t in range(number_steps):

            if s_T_WT1_cc[t] < T_min_th or (s_P_EH1_cc[-1] == self._P_max and s_T_WT1_cc[-1] < T_max_th):
                s_P_EH1_cc.append(self._P_max)
            else:
                s_P_EH1_cc.append(0)

            Q_loss = self._q_loss_per_K*(s_T_WT1_cc[-1] - self._T_ambient)  # kW losses

            T_new = s_T_WT1_cc[-1] + (s_P_EH1_cc[-1] - Q_loss - self._rhocp*(self._T_pref-self._T_in)*self._V_DHW[t]/3600)*60 / self._mcp
            s_T_WT1_cc.append(T_new)

        return s_T_WT1_cc, s_P_EH1_cc

    def real_control(self, number_steps, legionella_safety, forecast_type):

        self._last_search=0
        self._T_target_last=self._T_min

        if forecast_type=='Perfect':
            V_DHW=list(self._V_DHW)
        legionellenschaltung = 'OFF'
        last_legionella_eradication = 0
        cpp = max(self._importcost)
        nt = min(self._importcost)
        for i in range(2*1440):
            if nt<self._importcost[i]<cpp:
                ht=self._importcost[i]
                break
        HT_mode='OFF'
        DLC_mode = 'OFF'

        last_update = 0

        s_P_EH1_real = [0]
        s_T_WT1_real = [self._T_min]
        if self._tariff=='TOU':

            alpha=int(math.ceil((self._mcp/self._q_loss_per_K )*np.log(ht/nt)/60))

            if alpha>=960:
                alpha=960
            else:
                i=0
                LDend=0
                while LDend==0:
                    i+=1
                    if self._importcost[i] == cpp and self._importcost[i - 1] == ht:
                        LDstart = i
                    if self._importcost[i]==ht and self._importcost[i-1]==cpp:
                        LDend=i

                    if self._importcost[i]==ht and self._importcost[i-1]==nt:
                        htstart=i

                if LDend<=htstart+alpha<=LDstart:
                        alpha=LDend-htstart


        random_week=10  # as starting point for SES forecasting
        SES = self._V_DHW[10080*random_week:10080*(random_week+3)]
        self._fctime=0

        '''function to forecast DHW depending on forecast-type'''
        '''function to forecast DHW depending on forecast-type: naive, SES or MA'''

        def get_DHW_forecast(input, SES, t, last_update):
            smoothing_constant = 0.47
            if forecast_type == 'Naive':
                smoothing_constant = 0

            if forecast_type == 'MA':

                V_DHW_MA = input[:]
                if len(input) / 1440 < 7 * 15:
                    ma_duration = int(math.floor(len(input) / (7 * 15 * 1440)))
                else:
                    ma_duration = 7
                for i in range(8 * 1440):
                    if ma_duration >= 1:

                        As = []

                        for a in range(ma_duration):
                            As.append(input[-(1440) * (a + 1) + i])

                        MA = sum(As) / len(As)
                        V_DHW_MA.append(MA)
                    else:
                        V_DHW_MA.append(V_DHW_MA[-1440])

                dhw = list(V_DHW_MA)



            elif forecast_type == 'Perfect':
                dhw = list(self._V_DHW)
            else:
                '''Creating SES or Naive (naive: smoothing constant=0) forecast'''
                # existing forecast is erased
                SES = SES[:-(7 * 1440 - (t - last_update))]

                for n in range(30 * 1440):
                    SES.append(smoothing_constant * input[- 7 * 1440] + (1 - smoothing_constant) * SES[
                        - 7 * 1440])
                    input.append(SES[-1])

                dhw = list(input)
                '''perfect forecast'''

            return dhw, SES


        '''time it takes to go from Tmin to T'''
        def max_EH_time(t, T_targ):
            i = 0
            while T_targ > self._T_min:
                i += 1
                Q_dhw = self._rhocp*(self._T_pref-self._T_in) * V_DHW[t -i] / 60 -self._P_max*60
                T_targ = (-Q_dhw + self._q_loss_per_K * 60 * self._T_ambient - self._mcp * T_targ) / (
                        self._q_loss_per_K * 60 - self._mcp)
            return i

        def EH_time(T_current,t, T_targ):
            if T_targ<T_current:
                i=0
            else:
                i = 0
                while T_targ > T_current:
                    i += 1

                    Q_dhw = self._rhocp*(self._T_pref-self._T_in)* V_DHW[
                        t - i] / 60 - self._P_max * 60

                    T_targ = (-Q_dhw + self._q_loss_per_K * 60 * self._T_ambient - self._mcp * T_targ) / (
                            self._q_loss_per_K * 60 - self._mcp)
            return i

        self.ht_phase_ident=0
        '''function to find the HT phase with the highest DHWD between t+1440 and t+5*1440'''
        def optimal_legionella_eradication_HT_phase(last_legionella_eradication):
            HT_start_list = []
            T_HT_start_list = []
            eradication_effort_list = []

            m=0
            self.ht_phase_ident-=time.time()
            extra_time=EH_time(self._T_min,last_legionella_eradication+2*1440-25,60)

            '''finding all starting time-steps of HT periods in coming days'''
            while m<=5*1440-extra_time:
                if self._importcost[last_legionella_eradication+m+2*1440+extra_time] == ht and self._importcost[last_legionella_eradication+m - 1+2*1440+extra_time] == nt:
                    HT_start_list.append(last_legionella_eradication+m+2*1440+extra_time)
                m+=1


            for n in range(len(HT_start_list)):

                T_HT_start_list.append(T_HT_start_(HT_start_list[n],0))
                time_factor = 10080 / (float(HT_start_list[n] - last_legionella_eradication))
                eradication_effort=(60-T_HT_start_list[n])*time_factor

                eradication_effort_list.append(eradication_effort)


            eradication = HT_start_list[eradication_effort_list.index(min(eradication_effort_list))]
            duration = 960

            self.ht_phase_ident += time.time()

            return eradication, duration

        self.reg_phase_ident = 0

        def optimal_legionella_eradication_period(last_legionella_eradication):

            eradication=0
            minimum = 10**10
            extra_time = EH_time(self._T_min, last_legionella_eradication + 2*1440 , 60)+25
            for i in range(extra_time/10,5*1440/10):
                i=i*10
                T_ = [60]
                T_new=60
                current=0
                n=0
                self.reg_phase_ident -= time.time()
                while current < minimum and T_new > self._T_min:
                    n+=1
                    Q_loss = self._q_loss_per_K*(T_new - self._T_ambient)
                    T_new = T_new - ((self._rhocp*(self._T_pref-self._T_in)*V_DHW[n+i+last_legionella_eradication+2*1440]/3600 + Q_loss)*60) / self._mcp
                    T_.append(T_new)

                    current+=T_new-self._T_min
                self.reg_phase_ident += time.time()
                time_factor=10080/(2*1440+i)

                current=current*time_factor

                if current < minimum:

                    minimum=current
                    eradication=last_legionella_eradication + 2*1440+i

            return eradication, 1

        '''function to calculate Target temperature to be reached in order to compensate DHW and loss in the given horizon'''
        def T_tar(horizon, t):
            T_=self._T_min

            for n in range(horizon+1):
                Q_dhw = self._rhocp*(self._T_pref-self._T_in) * V_DHW[t+horizon-n] / 60
                T_ = (-Q_dhw + self._q_loss_per_K * 60 * self._T_ambient - self._mcp * T_) / (
                        self._q_loss_per_K * 60 - self._mcp)

            T_target = T_
            return T_target

        '''function to calculate the start and duration of the next HT period'''
        def next_HT_phase(t):
            k = 0
            j = 0
            while j < 48*60 and self._importcost[j + t] != ht:
                j += 1
            start_next_HT = j + t
            while k < 48*60 and self._importcost[start_next_HT + k] != nt:
                k += 1
            duration_next_HT = k
            return start_next_HT, duration_next_HT

        def next_DLC_phase(t):
            k = 0
            j = 0
            while j < 48*60 and self._importcost[j + t] != cpp:
                j += 1

            if j==48*60:
                start_next_DLC=V_DHW.index(V_DHW[-1])
            else:
                start_next_DLC = j + t
            while k < 48*60 and self._importcost[start_next_DLC + k] ==cpp:
                k += 1
            duration_next_DLC = k

            return start_next_DLC, duration_next_DLC

        '''Search for optimal temperature at the beginning of next HT period'''
        def T_DLC_start_(start_next_DLC, duration_next_DLC):

            horizon = horizon_(start_next_DLC + duration_next_DLC)

            T_target = T_tar(horizon, start_next_DLC + duration_next_DLC)

            T_DLC_start = T_target

            for i in range(duration_next_DLC):

                Q_dhw = self._rhocp*(self._T_pref-self._T_in) * V_DHW[start_next_DLC + duration_next_DLC - i] / 60

                T_DLC_start = (-Q_dhw + self._q_loss_per_K * 60 * self._T_ambient - self._mcp * T_DLC_start) / (
                        self._q_loss_per_K * 60 - self._mcp)

            return T_DLC_start

        '''Search for optimal time before the next HT period to start loading up'''
        self.HT1=0
        self.HT2=0
        self.HT3=0
        def T_HT_start_(start_next_HT, duration_next_HT):


            '''first check how much of the HT is actually pre-loadable'''

            T_HT_start = T_tar(alpha, start_next_HT)

            return T_HT_start

        '''function to calculate safe horizon'''

        pq=self._P_max/self._q_loss_per_K

        t_max=int(math.ceil((self._mcp/self._q_loss_per_K *np.log((pq-(self._T_min-self._T_ambient))/(pq-(self._T_max-self._T_ambient))))/60))
        tau = self._mcp / self._q_loss_per_K
        ex = math.exp(t_max / tau)
        def horizon_(t):
            Q_DHW=sum(V_DHW[t:t+t_max])/60*self._rhocp*(self._T_pref-self._T_in)
            t_h = (Q_DHW * ex + self._mcp *(self._T_min - self._T_ambient) * (ex-1)) / (60 * self._P_max)
            return int(math.ceil(t_h))

        def P_scheduling(T_last,step,legionellenschaltung,HT_mode,DLC_mode):
            if T_last > self._T_max:
                P_new=0
                
            elif self._tariff != 'Flat' and self._importcost[step] == cpp:
                P_new=0

            elif self._tariff != 'Flat' and HT_mode=='ON':
                    P_new = self._P_max

            elif self._tariff != 'Flat' and DLC_mode=='ON':
                P_new=self._P_max


            elif legionellenschaltung == 'ON' and T_last < 60.5:
                P_new = self._P_max
                
            else:
                horizon = horizon_(t)
                T_target= T_tar(horizon, t)

                if T_last < T_target:
                    P_new = self._P_max
                    
                else:
                    P_new=0

            return P_new

        self.leg_phase_ident = 0

        '''function to locate best period for legionella eradication'''
        def legionella_eradication(last_legionella_eradication):
            self.leg_phase_ident -= time.time()
            if self._tariff == 'Flat':
                next_eradication, duration = optimal_legionella_eradication_period(last_legionella_eradication)

            else:
                '''Compare Scenarios: Kill legionella before HT period or some other time'''
                '''Costs related to the legionella eradication in both scenarios are calculated and compared'''

                eradication_HT, duration_HT = optimal_legionella_eradication_HT_phase(last_legionella_eradication)
                eradication_regular, duration_regular = optimal_legionella_eradication_period(last_legionella_eradication)

                ''' Scenario 1: Heat up in the period with the highest DHW'''

                '''First, identify the time-period to be compared'''
                T_ = 60
                k = 0

                reg_analysis_start = eradication_regular - max_EH_time(eradication_regular - 25, 60)

                while T_ > self._T_min:
                    Q_loss = self._q_loss_per_K * (T_ - self._T_ambient)  # kW losses
                    T_ -= ((Q_loss + self._rhocp*(self._T_pref-self._T_in) * V_DHW[
                        eradication_regular + k] / 3600) * 60) / self._mcp
                    k += 1
                reg_analysis_end = reg_analysis_start + k
                if reg_analysis_end > len(V_DHW):
                    reg_analysis_end = len(V_DHW)

                '''  Scenario 1.1 & 1.2: Run in regular period with and without eradication'''
                T_regular_with_eradication = [self._T_min]
                P_regular_with_eradication = [0]
                T_regular_without_eradication = [self._T_min]
                P_regular_without_eradication = [0]

                cost_regular_with_eradication = []
                cost_regular_without_eradication = []
                erd = 0

                for n in range(reg_analysis_start, reg_analysis_end):

                    horizon = horizon_(n)
                    if horizon + n > reg_analysis_end:
                        horizon = n - reg_analysis_end

                    T_target = T_tar(horizon, n)

                    if T_regular_without_eradication[-1] < T_target:
                        P_regular_without_eradication.append(self._P_max)
                    else:
                        P_regular_without_eradication.append(0)

                    if erd == 0:

                        if len(T_regular_with_eradication) < 25 or sum(T_regular_with_eradication[-25:]) / 25 <= 60:
                            if T_regular_with_eradication[-1] < 62:
                                P_regular_with_eradication.append(self._P_max)
                            else:
                                P_regular_with_eradication.append(0)
                        elif sum(T_regular_with_eradication[-25:]) / 25 > 60:
                            erd = 1
                            P_regular_with_eradication.append(0)
                        else:
                            P_regular_with_eradication.append(self._P_max)
                    else:
                        if T_regular_with_eradication[-1] < T_target:
                            P_regular_with_eradication.append(self._P_max)
                        else:
                            P_regular_with_eradication.append(0)

                    T_regular_with_eradication.append(T_regular_with_eradication[-1] - (((self._q_loss_per_K * (
                            T_regular_with_eradication[-1] - self._T_ambient) + self._rhocp*(self._T_pref-self._T_in) * V_DHW[n] / 3600 -
                                                                                          P_regular_with_eradication[
                                                                                              -1]) * 60) / self._mcp))

                    T_regular_without_eradication.append(T_regular_without_eradication[-1] - (((self._q_loss_per_K * (
                            T_regular_without_eradication[-1] - self._T_ambient) + self._rhocp*(self._T_pref-self._T_in) * V_DHW[n] / 3600 -
                                                                                                P_regular_without_eradication[
                                                                                                    -1]) * 60) / self._mcp))

                for n in range(len(T_regular_without_eradication)):
                    price = self._importcost[reg_analysis_start + n]

                    if price == cpp:
                        price = ht

                    cost_regular_with_eradication.append(price * P_regular_with_eradication[n])
                    cost_regular_without_eradication.append(price * P_regular_without_eradication[n])

                cost_regular_eradication = (sum(cost_regular_with_eradication) - sum(
                    cost_regular_without_eradication)) / 6000


                '''Scenario 2 - Heat up before an HT period'''

                T_HT_start_normal = T_HT_start_(eradication_HT, duration_HT)

                if T_HT_start_normal >= 60:
                    next_eradication = eradication_HT

                else:

                    '''First, identify the time-period to be compared'''
                    T_ = 60
                    k = 0

                    HT_analysis_start = eradication_HT - max_EH_time(eradication_HT - 25, 60)

                    while T_ > self._T_min:
                        Q_loss = self._q_loss_per_K * (T_ - self._T_ambient)  # kW losses
                        T_ -= ((Q_loss + self._rhocp*(self._T_pref-self._T_in) * V_DHW[
                            eradication_HT + k] / 3600) * 60) / self._mcp
                        k += 1
                    HT_analysis_end = HT_analysis_start + k

                    '''  Scenario 2.1 & 2.2: Run before HT phase with and without eradication'''

                    if HT_analysis_end > len(V_DHW):
                        HT_analysis_end = len(V_DHW)

                    T_HT_with_eradication = [self._T_min]
                    P_HT_with_eradication = [0]
                    T_HT_without_eradication = [self._T_min]
                    P_HT_without_eradication = [0]

                    cost_HT_with_eradication = []
                    cost_HT_without_eradication = []

                    t_start_loadup_with_eradication = HT_analysis_start
                    t_start_loadup_without_eradication = eradication_HT - max_EH_time(eradication_HT - 25,
                                                                                      T_HT_start_normal)
                    for n in range(HT_analysis_start, HT_analysis_end):

                        horizon = horizon_(n)
                        if horizon + n > HT_analysis_end:
                            horizon = n - HT_analysis_end

                        T_target = T_tar(horizon, n)

                        if T_HT_without_eradication[-1] < T_target:
                            P_HT_without_eradication.append(self._P_max)
                        elif t_start_loadup_without_eradication <= n < eradication_HT:

                            P_HT_without_eradication.append(self._P_max)
                        else:
                            P_HT_without_eradication.append(0)

                        if T_HT_with_eradication[-1] < T_target:
                            P_HT_with_eradication.append(self._P_max)
                        elif t_start_loadup_with_eradication <= n < eradication_HT:
                            P_HT_with_eradication.append(self._P_max)

                        else:
                            P_HT_with_eradication.append(0)

                        T_HT_with_eradication.append(T_HT_with_eradication[-1] - (((self._q_loss_per_K * (
                                T_HT_with_eradication[-1] - self._T_ambient) + self._rhocp*(self._T_pref-self._T_in)* V_DHW[n] / 3600
                                                                                    - P_HT_with_eradication[
                                                                                        -1]) * 60) / self._mcp))
                        T_HT_without_eradication.append(
                            T_HT_without_eradication[-1] - (((self._q_loss_per_K * (
                                    T_HT_without_eradication[-1] - self._T_ambient) + self._rhocp*(self._T_pref-self._T_in) * V_DHW[n] / 3600 -
                                                              P_HT_without_eradication[-1]) * 60) / self._mcp))

                    for n in range(len(T_HT_without_eradication)):
                        price = self._importcost[HT_analysis_start + n]

                        if price == cpp:
                            price = ht

                        cost_HT_with_eradication.append(price * P_HT_with_eradication[n])
                        cost_HT_without_eradication.append(price * P_HT_without_eradication[n])

                    cost_HT_eradication = (sum(cost_HT_with_eradication) - sum(
                        cost_HT_without_eradication)) / 6000


                    '''Factoring in time'''

                    cost_regular_eradication = cost_regular_eradication * (
                            10080 / (eradication_regular - last_legionella_eradication))
                    cost_HT_eradication = cost_HT_eradication * (
                            10080 / (eradication_HT - last_legionella_eradication))

                    if cost_HT_eradication < cost_regular_eradication:
                        next_eradication = eradication_HT

                    else:
                        next_eradication = eradication_regular

            self.leg_phase_ident += time.time()
            start_next_eradication = int(math.ceil(next_eradication - max_EH_time(next_eradication,60)))-25
            return start_next_eradication

        '''LOOP'''
        for t in range(1,number_steps):

            '''FORECASTING'''
            if t==1 or t % 1440 == 0:


                if forecast_type=='SES' or forecast_type=='MA':
                    if t > 10080:
                        V_DHW, SES = list(get_DHW_forecast(self._V_DHW[:t], SES, t, last_update))
                        last_update = t
                    else:
                        inputweeks = list(self._V_DHW[10080*random_week:10080*(random_week + 4) + t])

                        V_DHW, SES = list(get_DHW_forecast(inputweeks, SES, t, last_update))
                        last_update = t
                elif forecast_type=='Naive':

                    if t>=10080:
                        V_DHW = list(V_DHW_Naive[:t+30*1440])

                    else:
                        V_DHW= list(self._V_DHW[10080*random_week:10080*(random_week + 4) + t])


            '''LEGIONELLA CONTROL'''

            if legionella_safety == 'ON':

                i = 1
                while s_T_WT1_real[-i] >= 60 and i <= 25:
                    i += 1
                if i >= 25:
                    legionellenschaltung = 'OFF'
                    last_legionella_eradication = t
                    start_next_eradication=V_DHW.index(V_DHW[-1])

                elif last_legionella_eradication + 2*1440 == t:

                    start_next_eradication = legionella_eradication(last_legionella_eradication)


                elif last_legionella_eradication + 2*1440 <= t and start_next_eradication == t:
                    legionellenschaltung = 'ON'


            if self._tariff != 'Flat':

                '''Planning next HT Period'''

                if t == 1 or (self._importcost[t] ==nt and self._importcost[t - 1] ==ht) or (self._importcost[t+1440] ==nt and self._importcost[t - 1+1440] ==ht):

                    start_next_HT, duration_next_HT = next_HT_phase(t)

                    T_HT_start = T_HT_start_(start_next_HT, duration_next_HT)

                    earliest_HT_loadup=start_next_HT - max_EH_time(start_next_HT, T_HT_start)

                if self._importcost[t] == ht:
                    HT_mode = 'OFF'
                elif self._importcost[t] == nt and HT_mode != 'ON' and earliest_HT_loadup <= t - 1 < start_next_HT:
                    if start_next_HT - EH_time(s_T_WT1_real[-1], start_next_HT, T_HT_start) <= t - 1 < start_next_HT:
                        HT_mode = 'ON'

                if t == 1 or self._importcost[t] !=cpp and self._importcost[t - 1] == cpp:

                    start_next_DLC, duration_next_DLC = next_DLC_phase(t)
                    T_DLC_start=T_DLC_start_(start_next_DLC, duration_next_DLC)
                    earliest_DLC_loadup = start_next_DLC - max_EH_time(start_next_DLC, T_DLC_start)

                if self._importcost[t] == cpp:
                    DLC_mode = 'OFF'
                else:
                    if DLC_mode == 'OFF' and earliest_DLC_loadup <= t:
                        loading_time_DLC=EH_time(s_T_WT1_real[-1], start_next_DLC, T_DLC_start)
                        if loading_time_DLC!=0 and start_next_DLC-loading_time_DLC-1<=t:
                            DLC_mode = 'ON'


            '''Scheduling'''
            P_new = P_scheduling(T_last=s_T_WT1_real[-1], step=t, legionellenschaltung=legionellenschaltung,HT_mode=HT_mode,DLC_mode=DLC_mode)

            '''Calculation based on resulting power schedule'''
            Q_loss = self._q_loss_per_K*(s_T_WT1_real[-1] - self._T_ambient)  # kW losses
            T_new = s_T_WT1_real[-1] + ((P_new - Q_loss - self._rhocp*(self._T_pref-self._T_in)*self._V_DHW[t]/3600)*60) / self._mcp
            s_P_EH1_real.append(P_new)
            '''produce timeline'''
            s_T_WT1_real.append(T_new)


        return s_T_WT1_real, s_P_EH1_real