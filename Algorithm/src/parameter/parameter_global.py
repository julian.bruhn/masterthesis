# -*- coding: utf-8 -*-
# =============================================================================
#   AUTHOR:	Baptiste Feron bferon@eonerc.rwth-aachen.de
#   CREATED:    02.12.2016
#   LICENSE:    MIT
# =============================================================================
"""Global parameter defines the parameters used for the all simulation
"""
import os.path
import numpy as np
import logging

log = logging.getLogger(__name__)


class ParameterGlobal(object):
    """Parameter file with global parameters for the simulation

    Args:
    """

    def __init__(self, number_days, schedule_horizon_s, resolution_opti, resolution_simu, t_rescheduling_s,
                 t_max_MILP_solving_s, MILP_max_gap):
        """create an instance containing the global parameters

        Returns:
        """
        self._resolution_opti = resolution_opti  # schedule resolution in second
        self._resolution_simu = resolution_simu  # reolution of simulation used to assess the opti [s]

        self._number_days = number_days  # number of days considered
        self._time_steps = self._number_days * 24 * 3600 / self._resolution_opti  # number of time_steps
        self._t_rescheduling = t_rescheduling_s  # rescheduling time in second
        self._schedule_horizon_s = schedule_horizon_s  # schedule horizon in s
        # import energy price c€/kJ
        self._t_max_MILP_solving_s = t_max_MILP_solving_s
        self._MILP_max_gap = MILP_max_gap

        if t_rescheduling_s > schedule_horizon_s:
            log.warning("t_rescheduling is larger than schedule horizon: infeasible")
            print "WARNING: t_rescheduling is larger than schedule horizon: infeasible"

        if np.mod(t_rescheduling_s, float(self._resolution_opti)) == 0:
            self._number_rescheduling = int(self._number_days * 24 * 3600 / t_rescheduling_s)
            self._t_rescheduling_step = self._t_rescheduling / self._resolution_opti
        else:
            log.warning("t_rescheduling is not a multiple of resolution")
            self._number_rescheduling_step = self._number_days * 24 * 3600 / t_rescheduling_s
            self._t_rescheduling_step = self._t_rescheduling / self._resolution_opti

        if np.mod(schedule_horizon_s, float(self._resolution_opti)) == 0:
            self._schedule_horizon_step = schedule_horizon_s / self._resolution_opti
        else:
            log.warning("scheduling_horizon is not a multiple of resolution")
            self._schedule_horizon_step = schedule_horizon_s / self._resolution_opti

    def time_steps(self):
        """return time_steps

        Returns:
            - the number of considered time_steps for the all simulation
        """
        return self._time_steps

    def resolution(self):
        """return optimization resolution in second

        Returns:
            - the schedule resolution in second
        """
        return self._resolution_opti

    def resolution_simu(self):
        """return simulation resolution in second

        Returns:
            - the simulation resolution in second
        """
        return self._resolution_simu

    def number_days(self):
        """return number_days

        Returns:
            - the number odf considered days in days
        """
        return self._number_days

    def t_rescheduling_step(self):
        """return t_rescheduling in number of steps

        Returns:
            - the rescheduling time in number of steps
        """
        return self._t_rescheduling_step

    def t_rescheduling_s(self):
        """return t_rescheduling in second

        Returns:
            - the rescheduling time in second
        """
        return self._t_rescheduling

    def number_rescheduling(self):
        """return the number of rescheduling

        Returns:
            - the number of rescheduling all along the simulated time
        """
        return self._number_rescheduling

    def schedule_horizon_step(self):
        """return the schedule horizon in number of steps

        Returns:
            - the schedule horizon in number of steps
        """
        return self._schedule_horizon_step

    def t_schedule_horizon_s(self):
        """return the schedule horizon in second

        Returns:
            - the schedule horizon in second
        """
        return self._schedule_horizon_s

    def MILP_max_gap(self):
        """return the maximum gap allowed

        Returns:
            - MILP gap allowed
        """
        return self._MILP_max_gap

    def t_max_MILP_solving_s(self):
        """return the maximum time allowed for optimizing

        Returns:
            - MILP maximum time allowed for optimizing
        """

        return self._t_max_MILP_solving_s
