# -*- coding: utf-8 -*-
# =============================================================================
#   AUTHOR:	Baptiste Feron bferon@eonerc.rwth-aachen.de
#   CREATED:    02.12.2016
#   LICENSE:    MIT
#   FILE:	scen_reduction.py
# =============================================================================
"""It aims at reducing a high number of scenarios into few scenarios embedding all the other scenarios
This implementation is based on the Conejo book and the Kantorovich distance
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import itemfreq

def scenario_reduction(scenario_list,number_selected_scenario):
    """Return reduced scenario position and its associated probability.
    Based on the scenario reduction described in Conejo book: fast forward Kantotorovich method (p 100).
    This approach is an heuristic optimization which does not ensure an optimal solution but performs well in practice
    and is robust.
    The scenario reduction principle:
    1) At every loop, it calculates the norm distance between every scenario and select a representative scenario.
    A scenario is a time series. Given that, the norm distance between 2 scenarios i and j is:
    dij=(sum_t (s_i(t)-s_j(t))^2)^0.5
    Where t is the time step, s_i is the time serie i and s_j the j-th.
    Smaller is the sum of all the distances of a given scenario with the other one, more representative, it is.
    2) This scenario is then selected and stored in the omega_selected list. Its representative probability is then updated.
    At the end, the most representative scenario indexes are in the list omega_selected with their associated probability

    Args:
        scenario_list (numpy, list(float)) is a numpy or a list containing all the initial scenarios.
        [[s1(t0),...s1(tend)], 
        number_selected_scenario: number of selected scenarios at the end of the reduction
    Returns:
        omega_selected_index (list(integer)): contains the index of the selected scenarios
        probability (list(float)): contains the associated probability of the scenarios selected
    """

    if isinstance(scenario_list,list):
        #the list has to be transformed into a numpy form
        scen = np.asarray(scenario_list)
        #print type(scenario_list)
        #print(scenario_list)

    nbr_scen = len(scen)

    "1) Calcul of the initial norm matrix"
    omega_0 = range(nbr_scen)   # contains the index of non selected scenarios
    scen_0 = scen               # contains the scenario considered
    select_index, norm_matrix_0 = _index_selected(scen_0) #return the cost matrix index minimizing the distance with
    # all the others and the norm matrix
    # print select_index
    #print 'Original norm matrix: ', norm_matrix_0

    # list of selected scenario indices
    omega_selected_index = []
    # add the scenario index minimizing all the norms to scenario list
    omega_selected_index.append(select_index)
    "2) Initial selection of the scenario minimizing the sum of all the distance with the other scenarios"
    # initialise previous values for iteration
    selec_index_prev = select_index
    norm_matrix_prev = norm_matrix_0
    omega_unselected_index = omega_0
    omega_unselected_index.remove(select_index)

    # print omega_unselected_index

    while len(omega_selected_index) < number_selected_scenario:

        "1) Calcul of the norm matrix"
        norm_matrix_current = _update_norm_matrix(norm_matrix_prev,selec_index_prev)
        print "norm_current:",norm_matrix_current


        "2) Selection of the scenario minimizing the sum of all the distance with the other scenarios"
        # calculate probability distance of every scenario considered: if omega_select=3, d_1= norm(2,1)+norm(4,1)
        distance = []
        #print "length of omega_prev: %s" % len(omega_prev)
        #print "omega prev: ",omega_prev
        for scen in omega_unselected_index:
            omega_without_s = list(omega_unselected_index)
            omega_without_s.remove(scen)

            # calculation of the probability distance
            distance.append(norm_matrix_current[omega_without_s,scen].sum())
            # print distance

        # based on the distance for every scenario, the new scenario is selected
        new_scen_selected = omega_unselected_index[np.argmin(distance)]
        # print "new selected index: ",new_scen_selected

        # update omega containing the index sets
        omega_selected_index.append(new_scen_selected)
        omega_unselected_index.remove(new_scen_selected)

        # initiaise prev values for next iteration
        selec_index_prev = new_scen_selected
        norm_matrix_prev = norm_matrix_current

    # print 'Final selected scenarios: ', omega_selected_index
    # print 'Final unselected scnenarios: ', omega_unselected_index

    "3) Calcul of the probability associated to every selected scenarios"
    scen_probability = 1/float(nbr_scen)
    #print "Number of original Scenarios: %s. SAA Probability: %s." %(N_S,SAA_probability)
    probability_selected = _assign_probabilities(omega_selected_index,omega_unselected_index,norm_matrix_0,scen_probability)

    #reduced_array = reduce_array(array,omega_selected)
    # print " # Selected Set # Probabilities \n # %s # %s "%(omega_selected_index,probability_selected)

    return omega_selected_index,probability_selected

def _index_selected(scenarios):
    """Return the scenario minimizing the Kantorovich distance with the other scenarios
    1. it calculates the Kantorovich distance between every scenarios and select a representative scenario.
    A representative scenario = scenario minimizing the  Kantorovich distance (norm) with the other scenarios
    A scenario is a time series. Given that, the Kantorovich distance (norm_matrix) : dij=(sum_t (s_i(t)-s_j(t))^2)^0.5
    Where t is the time step, s_i is the time serie i and s_j the j-th.
    2. It selects the scenario index min the distance of a given scenario with other ones.

    Args:
        scenario(numpy (float)) is a numpy containing all the scenarios.
        [[s1(t0),...s1(tend)], [s2(t0),...,s2(tend)]...]
    Returns:
        selected_index (list(integer)): contains the index of the selected scenarios
        probability (list(float)): contains the associated probability of the scenarios selected
    """
    norm_matrix = _norm_matrix_calc(scenarios)
    selected_index = _select_scenario(norm_matrix)
    return selected_index, norm_matrix



def _norm_matrix_calc(scenario):
    """ INITIAL STEP 1:
    calculate the norm distance between different scenarios

    Args:
        scenario (numpy(nbr_scenario X nbr_time_steps)):

    Returns:
        norm_matrix(numpy[nbr_scenario X nbr_scenario]): norm matrix dij=(sum_t (s_i(t)-s_j(t))^2)^0.5
    """
    # row dimension (=Scenarios)
    nbr_scen = len(scenario)

    norm_matrix=np.zeros([nbr_scen,nbr_scen])

    for scen_i in range(nbr_scen):
        for scen_j in range(nbr_scen):
            difference_vec = scenario[scen_i,:]-scenario[scen_j,:]  #calauclate the difference between scen_i and scen_j
            norm_matrix[scen_i,scen_j]=np.linalg.norm(difference_vec)
    return norm_matrix

def _select_scenario(norm_matr):
    """ INITIAL STEP 2:
    select the scenario which minimizes the sum of all its norms difference with the others scenarios

    Args:
        norm_matrix(numpy[nbr_scenario X nbr_scenario]): norm matrix dij=(sum_t (s_i(t)-s_j(t))^2)^0.5

    Returns:
        min_scenario_index (int): index of the scenario minimizing the probability distance

    """

    nbr_scen = len(norm_matr)
    sum_norm = np.zeros(nbr_scen)
    for scen_j in range(nbr_scen):
        sum_norm[scen_j] = np.sum(norm_matr[scen_j,:]) #calculate the sum of all the norm difference of a given scenario j
    min_scenario_index = sum_norm.argmin()
    return min_scenario_index

def _update_norm_matrix(norm_matrix,selected_index):
    """ STEP 1:
    update the norm matrix based on the new selected scenario. For this reason, it selects the minimum value
    between the norm(scenario considered, other scenario not selected) and norm(scenario considered,
     new selected scenario)

    Args:
        norm_matrix(numpy[nbr_scenario X nbr_scenario]): norm matrix dij=(sum_t (s_i(t)-s_j(t))^2)^0.5
        selected_index:  index of the scenario minimizing the probability distance

    Returns:
        updated_norm_matrix (numpy[nbr_scenario X nbr_scenario]): norm matrix considering the new selected scenario
    """
    updated_norm_matrix = np.empty_like(norm_matrix)
    dim = len(norm_matrix)

    for scen_i in range(dim):
        for scen_j in range(dim):
            updated_norm_matrix[scen_i,scen_j] = np.minimum(norm_matrix[scen_i,scen_j],norm_matrix[scen_i,selected_index])
    return updated_norm_matrix


def _assign_probabilities(omega_selected,omega_unselected,norm_original,scen_probability):
    ''' STEP 3:
    It calculates the new probability of the reduced sets of selected scenarios.
    Based on the original norm matrix, we can see how far are scenarios from each others. The probability
    of a non selected scenario is then assigned to its closest selected scenario.
    Args:
        omega_selected (list(int)): contains all the selected scenario
        omega_unselected (list(int)): contains all the unselected scenario
        norm_original (numpy[nbr_scenario X nbr_scenario]): first calculated norm matrix dij=(sum_t (s_i(t)-s_j(t))^2)^0.5
        scen_probability (float): probabiltity of every scenario

    Returns:
        probability_selected_scen (list(float)):A list with the probability for every selected scenario

    '''
    "Select and store (index_associated_scenario) the index of the closest selected scenario (scen_index_min_norm)"
    # distribute probabilities from unselected to selected scenarios
    index_associated_scenario = []
    for scen in omega_unselected:
        # return the sorted index of the closest scenarios of the considered scenario
        sorted_index_min_norm = norm_original[:,scen].argsort()
        min_distance_index = None
        for scen_index_min_norm in sorted_index_min_norm:
            # if the first closest scenario is in the reduced set of selected scenario, then it is added to this scen.
            if scen_index_min_norm in omega_selected and min_distance_index == None:
                # print scen_index_min_norm
                min_distance_index = scen_index_min_norm
        #print 'unselected scenario: '+str(scen)+' associated to selected scenario: '+str(min_distance_index)+''
        # stores the index of the closest selected scenario
        index_associated_scenario.append( min_distance_index )
    #print index_associated_scenario

    "Based on the index of the closest selected scenario associated to every unselected scenario, the prob is derived"
    # returns a matrix with the number of closer unselected scenarios for every selected scenarios index
    # [[scen_2, 4 occur], [scen_4, 2 occur]]
    freq = itemfreq(index_associated_scenario)
    # print "Frequency array \n",freq
    probability_selected_scen=[]
    for index in omega_selected:
        probability = scen_probability
        for i in range(len(freq)):
            if index==freq[i,0]:
                #print "index %s has frequency %s" % (index,freq[i,1])
                #print "Adding %s SAA probabilites to scenario %s" % (freq[i,1],index)
                probability+=freq[i,1]*scen_probability
        probability_selected_scen.append( probability )

    return probability_selected_scen
