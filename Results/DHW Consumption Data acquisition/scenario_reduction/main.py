from scenario_reduction import scenario_reduction



import os
import glob
import csv


extension = 'csv'
os.chdir(os.getcwd()+'/DHWfiles')
result = [i for i in glob.glob('*.{}'.format(extension))]

scenario_list=[]



names_liste=[i for i in glob.glob('*.{}'.format(extension))]#
#print names_liste
for k in range(len(names_liste)):

        new_liste = []
        test_file = csv.reader(
            open(os.getcwd() + '/%s' % names_liste[k]), delimiter=",")
        for entry in test_file:
            new_liste.append(float(entry[0]))

        scenario_list.append(new_liste)




number_selected_scenario=1

"scenario_list (numpy(float) or list(float)) is a numpy or a list containing all the initial scenarios. " \
"[[s1(t0),...s1(tend)], [s2(t0),...,s2(tend)]...]"

scenarios=scenario_list #[range(2,12), range(3,13), [4, 5, 6, 7, 8, 9, 10, 11, 14, 25], [4, 5, 6, 7, 0, 9, 10, 11, 9, 15]]



omega_selected_index,probability_selected = scenario_reduction(scenarios,number_selected_scenario)

#print 'index of selected scenarios: ', omega_selected_index
#print 'probability of these scenarios: ', probability_selected

print 'Selected file: '+str(names_liste[omega_selected_index[0]])
