## Content

- This folder contains a scenario reduction algorithms based on Conejo book in the folder: p. 100
- To run it: run main.py
- To run the scenario reduction with your own data: update "scenarios" list
- To change the number of selected scenarios: update "number_selected_scenario"